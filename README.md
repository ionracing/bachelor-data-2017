# Sammendrag

En side sammendrag av hele bacheloren
# Forord (ikke nødvendig)

Hvem er vi og takker de som har hjelpet oss
# Innhodsfortegnelsen
# Liste over figurer
# Liste over forkortelser
# Introduksjon – oppgavetekst 
*Oppgavetekst

# Introduksjon – bakgrunn
* Motivasjon
  * ION Racing
  * Formula Student
  * Tidligere år i ION?
  * Behov for datainnsamling/analyse
* Teori
  * Tanke bak systemoppbygging (bil -> RPi -> klienter)
    * I bilen
      * Hvordan elektrokikken i bilen ser ut
      * Litt om Microcontoller
        * Valg av programmeringspråk
      * Sensorer i bilen
      * Sensonsorinnhenting
    * RPi
      * Server
        * Valg av programmeringspråk
      * Klient
      * Hvorfor web
      * Valg av programmeringspråket
      * Funksjonalitet
      * Design (brukerundersøkelser her?)

# Konstruksjon
* Redgjørelse av det som er gjort
  * Lavnivå
    * 
  * Høynivå
    * Server-side
    * Client-side
* Forklaring av program/system på overordnet nivå

# Resultat
* Tester ute på bane, og inne etterpå ved analyse. (Bra hvis vi får virkelige hendelser som har hjelpet å gjøre bilen bedre.)

# Diskusjon
* Sammenligning av resultatene med oppgaveteksten
* Alternative løsninger
* Mulige forbedringer
* Konklusjon (kort)

# Vedlegg
* Sier seg selv
